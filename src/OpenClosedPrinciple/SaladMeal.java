package OpenClosedPrinciple;

public class SaladMeal implements IMeal {
    @Override
    public void Make() {
        System.out.println("Began the process of cooking salad");
    }
}
