package OpenClosedPrinciple;

public class PotatoMeal implements IMeal {
    @Override
    public void Make() {
        System.out.println("Began the process of cooking potatoes");
    }
}
