package OpenClosedPrinciple;

public class Cook {
    private String cooksName;

    public Cook(String cooksName) {
        this.cooksName = cooksName;
    }

    public void makeDinner(IMeal meal)
    {
        System.out.println("Cook`s name: " + cooksName);
        meal.Make();
    }
}
