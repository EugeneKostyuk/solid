import DependencyInjectionsPrinciple.Car;
import DependencyInjectionsPrinciple.CryEngine;
import InterfaceSegregationPrinciple.TouchListener;
import InterfaceSegregationPrinciple.Window;
import LiskovSubstitutionPrinciple.Rectangle;
import LiskovSubstitutionPrinciple.Shape;
import LiskovSubstitutionPrinciple.Square;
import OpenClosedPrinciple.Cook;
import OpenClosedPrinciple.PotatoMeal;
import OpenClosedPrinciple.SaladMeal;
import SingleResponsibilityPrinciple.Account;
import SingleResponsibilityPrinciple.Person;

public class Main {

    public static void main(String[] args) {
        //Single Responsibility
        System.out.println("Single Responsibility Principle");
        Person bob = new Person();
        bob.addAccount(new Account());

        //Open-closed
        System.out.println("Open closed Principe");
        Cook cook = new Cook("Eugene");
        cook.makeDinner(new PotatoMeal());
        cook.makeDinner(new SaladMeal());

        //Liskov Substitution
        System.out.println("Liskov Substitution Principle");
        Shape rectangle = new Rectangle(3, 6);
        System.out.println("Rectangle`s area = " + rectangle.getArea());

        Shape square = new Square(5);
        System.out.println("Square`s area = " + square.getArea());

        //Interface Segregation
        System.out.println("Interface Segregation Principle");
        Window window = new Window();
        window.setTouchListener(new TouchListener() {
            @Override
            public void onDown(int x, int y) {
                //to do something
            }

            @Override
            public void onMove(int x, int y) {
                //to do something
            }

            @Override
            public void onUp() {
                //to do something
            }
        });

        //Dependency injections
        System.out.println("Dependency injections Principe");
        Car car = new Car(new CryEngine());
        car.startEngine();
        car.stopEngine();
    }
}
