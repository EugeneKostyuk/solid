package InterfaceSegregationPrinciple;

public interface MouseMotionListener {
    void onMouseDragged();
    void onMouseMoved();
}
