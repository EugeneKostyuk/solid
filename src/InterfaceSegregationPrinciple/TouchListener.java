package InterfaceSegregationPrinciple;

public interface TouchListener {
    void onDown(int x, int y);
    void onMove(int x, int y);
    void onUp();
}
