package InterfaceSegregationPrinciple;

public class Window {
    private MouseMotionListener mouseMotionListener;
    private KeyListener keyListener;
    private TouchListener touchListener;

    public void setMouseMotionListener(MouseMotionListener mouseMotionListener) {
        this.mouseMotionListener = mouseMotionListener;
    }

    public void setKeyListener(KeyListener keyListener) {
        this.keyListener = keyListener;
    }

    public void setTouchListener(TouchListener touchListener) {
        this.touchListener = touchListener;
    }
}
