package InterfaceSegregationPrinciple;

public interface KeyListener {
    void onKeyDown(int key);
    void onKeyPressed(int key);
    void onKeyUp(int key);
}
