package DependencyInjectionsPrinciple;

public class PowerEngine implements IEngine {
    @Override
    public void start() {
        System.out.println("Start Power Engine...");
    }

    @Override
    public void stop() {
        System.out.println("Stop Power Engine...");
    }
}
