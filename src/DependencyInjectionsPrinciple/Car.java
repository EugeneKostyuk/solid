package DependencyInjectionsPrinciple;

public class Car {
    private IEngine engine;

    public Car(IEngine engine) {
        this.engine = engine;
    }

    public void startEngine() {
        this.engine.start();
    }

    public void stopEngine() {
        this.engine.stop();
    }
}
