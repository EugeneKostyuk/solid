package DependencyInjectionsPrinciple;

public class CryEngine implements IEngine {
    @Override
    public void start() {
        System.out.println("Start Cry Engine...");
    }

    @Override
    public void stop() {
        System.out.println("Stop Cry Engine...");
    }
}
