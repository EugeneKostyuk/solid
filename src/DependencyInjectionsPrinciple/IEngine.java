package DependencyInjectionsPrinciple;

public interface IEngine {
    void start();
    void stop();
}
