package LiskovSubstitutionPrinciple;

public interface Shape {
    float getArea();
}
