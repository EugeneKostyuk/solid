package LiskovSubstitutionPrinciple;

public class Square implements Shape {
    private float sideSize;

    public Square() {
    }

    public Square(float sideSize) {
        this.sideSize = sideSize;
    }

    public float getSideSize() {
        return sideSize;
    }

    public void setSideSize(float sideSize) {
        this.sideSize = sideSize;
    }

    @Override
    public float getArea() {
        return sideSize * sideSize;
    }
}
